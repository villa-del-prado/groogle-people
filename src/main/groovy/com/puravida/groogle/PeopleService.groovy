package com.puravida.groogle

import groovy.transform.CompileStatic
import com.google.api.services.people.v1.model.*
import java.util.function.Consumer

@CompileStatic
interface PeopleService extends Groogle.GroogleService{

    interface AddressDSL{
        AddressDSL city(String city)
        AddressDSL country(String country)
    }

    interface NicknameDSL{
        NicknameDSL type(String type)
        NicknameDSL value(String value)
    }

    interface WithPerson {
        Person person()

        WithPerson addresses(Address...s)
        WithPerson biographies(Biography...s)
        WithPerson birthdays(Birthday... s)
        WithPerson emailAddresses(EmailAddress... s)
        WithPerson events(Event... s)
        WithPerson genders(Gender... s)
        WithPerson imClients(ImClient... s)
        WithPerson interests(Interest... s)
        WithPerson locales(Locale... s)
        WithPerson names(Name... s)
        WithPerson nicknames(Nickname... s)
        WithPerson occupations(Occupation... s)
        WithPerson organizations(Organization... s)
        WithPerson phoneNumbers(PhoneNumber... s)
        WithPerson relations(Relation... s)
        WithPerson residences(Residence... s)
        WithPerson sipAddresses(SipAddress... s)
        WithPerson urls(Url... s)
        WithPerson userDefined(UserDefined... s)

        WithPerson addAddress(Consumer<AddressDSL>consumer)
        WithPerson removeAddress(Consumer<AddressDSL>consumer)

        WithPerson addNickname(Consumer<NicknameDSL>consumer)
        WithPerson removeNickname(Consumer<NicknameDSL>consumer)

        void delete()
    }

    interface WithPeople{
        WithPeople onlyRetrieve(String ...fields)
        WithPeople byModified()
        WithPeople byFistName()
        WithPeople byLastName()
        WithPeople batchSize(int batchSize)
    }

    PeopleService me( Consumer<WithPerson> withPersonConsumer)

    List<Person> allPeople()

    PeopleService eachPeople( Consumer<WithPeople> filter, Consumer<WithPerson> withPersonConsumer)

    PeopleService eachPeople( Consumer<WithPerson> withPersonConsumer)

    PeopleService withPerson( Person person, Consumer<WithPerson> withPersonConsumer)

    PeopleService createPerson( Consumer<WithPerson> withPersonConsumer)
}