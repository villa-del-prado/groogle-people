package com.puravida.groogle.impl

import com.google.api.services.people.v1.model.Address
import com.puravida.groogle.PeopleService
import groovy.transform.CompileStatic

@CompileStatic
class AddressSpec implements PeopleService.AddressDSL{

    Address address

    @Override
    PeopleService.AddressDSL city(String city) {
        address.setCity(city)
        this
    }

    @Override
    PeopleService.AddressDSL country(String country) {
        address.setCountry(country)
        this
    }

    boolean isEqualTo( Address b){
        if( address.getCity() && b.getCity() && address.getCity()!=b.getCity())
            return false
        if( address.getCountry() && b.getCountry() && address.getCountry()!=b.getCountry())
            return false
        true
    }

}
