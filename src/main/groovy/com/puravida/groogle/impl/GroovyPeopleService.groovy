package com.puravida.groogle.impl

import com.google.api.client.auth.oauth2.Credential
import com.google.api.client.http.HttpTransport
import com.google.api.client.json.JsonFactory
import com.google.api.services.people.v1.PeopleService as PS
import com.google.api.services.people.v1.model.*
import com.puravida.groogle.PeopleService
import groovy.transform.CompileStatic

import java.util.function.Consumer

@CompileStatic
class GroovyPeopleService implements PeopleService, InternalService{

    PS service

    @Override
    void configure(JsonFactory jsonFactory, HttpTransport httpTransport, Credential credential, String applicationName) {
        this.service = new com.google.api.services.people.v1.PeopleService.Builder(httpTransport, jsonFactory, credential)
                .setApplicationName(applicationName)
                .build()
    }

    @Override
    List<Person> allPeople() {
        List<Person> ret = []
        eachPeople( new Consumer<WithPeople>() {
            @Override
            void accept(WithPeople withPeople) {

            }
        }, new Consumer<WithPerson>() {
            @Override
            void accept(WithPerson withPerson) {
                ret.add withPerson.person()
            }
        })
        ret
    }


    @Override
    PeopleService me(Consumer<WithPerson> withPersonConsumer) {
        Person me = service.people().get("people/me").setPersonFields(WithPeopleSpec.allFields).execute()
        WithPersonSpec withPeopleSpec = new WithPersonSpec(service: service, person:me)
        withPersonConsumer.accept(withPeopleSpec)
        if( withPeopleSpec.dirty )
            withPeopleSpec.update()
        this
    }

    @Override
    PeopleService eachPeople(Consumer<WithPerson> withPersonConsumer) {
        eachPeople(new Consumer<WithPeople>() {
            @Override
            void accept(WithPeople withPeople) {
                withPeople.onlyRetrieve(WithPeopleSpec.allFields)
            }
        }, withPersonConsumer)
        this
    }

    @Override
    PeopleService eachPeople(Consumer<WithPeople> filter, Consumer<WithPerson> withPersonConsumer) {
        WithPeopleSpec withPeopleSpec = new WithPeopleSpec(service: service)
        filter.accept(withPeopleSpec)
        withPeopleSpec.executeBatchPeople(withPersonConsumer)
        this
    }

    @Override
    PeopleService withPerson(Person person, Consumer<WithPerson> withPersonConsumer) {
        WithPersonSpec withPersonSpec = new WithPersonSpec(service: service, person:person)
        withPersonConsumer.accept(withPersonSpec)
        withPersonSpec.update()
        this
    }

    @Override
    PeopleService createPerson(Consumer<WithPerson> withPersonConsumer) {
        WithPersonSpec withPersonSpec = new WithPersonSpec(service: service, person: new Person())
        withPersonConsumer.accept(withPersonSpec)
        withPersonSpec.create()
        this
    }
}
