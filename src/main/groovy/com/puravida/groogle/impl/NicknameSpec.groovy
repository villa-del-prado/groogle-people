package com.puravida.groogle.impl

import com.google.api.services.people.v1.model.Address
import com.google.api.services.people.v1.model.Nickname
import com.puravida.groogle.PeopleService
import groovy.transform.CompileStatic

@CompileStatic
class NicknameSpec implements PeopleService.NicknameDSL{

    Nickname nickname

    @Override
    PeopleService.NicknameDSL type(String type) {
        nickname.type = type
        this
    }

    @Override
    PeopleService.NicknameDSL value(String value) {
        nickname.value = value
        this
    }

    boolean isEqualTo( Nickname b){
        if( nickname.getType() && b.getType() && nickname.getType()!=b.getType())
            return false
        if( nickname.getValue() && b.getValue() && nickname.getValue()!=b.getValue())
            return false
        true
    }

}
