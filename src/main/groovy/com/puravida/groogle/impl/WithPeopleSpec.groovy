package com.puravida.groogle.impl

import com.google.api.services.people.v1.model.ListConnectionsResponse
import com.google.api.services.people.v1.model.Person
import com.puravida.groogle.PeopleService
import groovy.transform.CompileStatic

import java.util.function.Consumer

@CompileStatic
class WithPeopleSpec implements PeopleService.WithPeople{

    com.google.api.services.people.v1.PeopleService service

    String peopleId = 'me'

    static String getAllFields() {
        [
                'addresses',
                'ageRanges',
                'biographies',
                'birthdays',
                'braggingRights',
                'coverPhotos',
                'emailAddresses',
                'events',
                'genders',
                'imClients',
                'interests',
                'locales',
                'memberships',
                'metadata',
                'names',
                'nicknames',
                'occupations',
                'organizations',
                'phoneNumbers',
                'photos',
                'relations',
                'relationshipInterests',
                'relationshipStatuses',
                'residences',
                'sipAddresses',
                'skills',
                'taglines',
                'urls',
                'userDefined',
        ].join(',')
    }

    List fields = ['names']

    @Override
    PeopleService.WithPeople onlyRetrieve(String... retrieve) {
        fields.addAll(retrieve)
        this
    }

    String orderBy = 'LAST_MODIFIED_ASCENDING'

    @Override
    PeopleService.WithPeople byFistName() {
        orderBy = 'FIRST_NAME_ASCENDING'
        this
    }

    @Override
    PeopleService.WithPeople byLastName() {
        orderBy = 'LAST_NAME_ASCENDING'
        this
    }

    @Override
    PeopleService.WithPeople byModified() {
        orderBy = 'LAST_MODIFIED_ASCENDING'
        this
    }


    int batchSize
    @Override
    PeopleService.WithPeople batchSize(int batchSize) {
        this.batchSize = batchSize
        this
    }

    void executeBatchPeople(Consumer<PeopleService.WithPerson> consumer, String pageToken=null){
        com.google.api.services.people.v1.PeopleService.People.Connections.List request = service.people().connections()
                .list("people/$peopleId")
                .setPersonFields(fields.join(','))
                .setSortOrder(orderBy)
                .setPageToken(pageToken)
                .setPageSize(batchSize)
        ListConnectionsResponse response = request.execute()
        response.connections?.each{ Person person->
            WithPersonSpec withPeopleSpec = new WithPersonSpec(service: service, person:person)
            consumer.accept(withPeopleSpec)
            if(withPeopleSpec.dirty)
                withPeopleSpec.update()
        }
        if( response.getNextPageToken() ) {
            executeBatchPeople(consumer, response.getNextPageToken())
        }
    }

}
