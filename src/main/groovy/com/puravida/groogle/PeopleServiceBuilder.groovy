package com.puravida.groogle

import com.puravida.groogle.impl.GroovyPeopleService
import groovy.transform.CompileStatic

@CompileStatic
class PeopleServiceBuilder {

    static PeopleService build(){
        new GroovyPeopleService()
    }

}
