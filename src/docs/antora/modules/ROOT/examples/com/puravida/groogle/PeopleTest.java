package com.puravida.groogle;

import com.google.api.services.people.v1.PeopleServiceScopes;
import com.google.api.services.people.v1.model.Nickname;
import com.google.api.services.people.v1.model.Person;
import com.sun.org.apache.xpath.internal.operations.String;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

@FixMethodOrder(MethodSorters.JVM)
public class PeopleTest {

    Groogle groogle;

    PeopleService peopleService;

    @Before
    public void setUp() {
        //tag::register[]
        groogle = GroogleBuilder.build(groogle -> {
            groogle
                    .credentials(credentials ->
                            credentials
                                    .applicationName("test-people")
                                    .withScopes(PeopleServiceScopes.CONTACTS,PeopleServiceScopes.USERINFO_PROFILE)
                                    .usingCredentials("src/test/resources/client_secret.json")
                                    .asService(false)
                                    .storeCredentials(true)
                    )
                    .service(PeopleServiceBuilder.build(), PeopleService.class);
        });
        peopleService = groogle.service(PeopleService.class);
        //end::register[]
        assert peopleService != null;
    }

    @Test
    public void registerService(){
    }

    @Test
    public void me(){
        peopleService.me( (PeopleService.WithPerson withPerson)->{
            System.out.println(withPerson.person().getNames());
        });
    }

    @Test
    public void allPeople(){
        for(Person p : peopleService.allPeople()){
            System.out.println(p.getNames());
        };
    }

    @Test
    public void listAllConnections(){
        final AtomicInteger count = new AtomicInteger();
        peopleService.eachPeople((PeopleService.WithPeople filter)->{

            filter.byFistName().batchSize(20);

        }, (PeopleService.WithPerson withPerson)->{

            System.out.println(withPerson.person().getNames());

        });
    }

    @Test
    public void createContact() {
        peopleService.createPerson( w->{
            Nickname nn = new Nickname();
            nn.setType("DEFAULT");
            nn.setValue("pelele");
            w.nicknames(nn);
        });
    }

    @Test
    public void deleteContact() {
        final List<Person> remove = new ArrayList<>();
        peopleService.eachPeople( w->{
            if( w.person().getNicknames() != null) {
                if (w.person().getNicknames().size() == 1) {
                    if (w.person().getNicknames().get(0).getValue() == "pelele") {
                        remove.add(w.person());
                    }
                }
            }
        });
        if( remove.size() == 0)
            return;

        for( Person pelele : remove) {

            peopleService.withPerson(pelele, w -> {
                w.delete();
            });

        }

        remove.clear();
        peopleService.eachPeople( w->{
            if( w.person().getNicknames() != null) {
                if (w.person().getNicknames().size() == 1) {
                    if (w.person().getNicknames().get(0).getValue() == "pelele") {
                        remove.add(w.person());
                    }
                }
            }
        });
        assert remove.size() == 0;
    }

        @Test
    public void updateContact(){

        //current
        final Person first = peopleService.allPeople().get(0);
        final List<Nickname> initial = first.getNicknames();

        //update
        peopleService.withPerson(first, w->{
            w.addNickname(
                    n-> n
                            .type("DEFAULT")
                            .value("pelele")
            );
        });

        //check
        peopleService.withPerson(first, w->{
            assert w.person().getNicknames().size() == 1;
            assert w.person().getNicknames().get(0).getValue().equals("pelele");
            assert w.person().getNicknames().get(0).getType().equals("DEFAULT");
        });

        //restore
        peopleService.withPerson(first, w->{
            if( initial == null)
                w.nicknames(null);
            else
                w.nicknames(initial.toArray(new Nickname[initial.size()]));
        });

    }

    @Test
    public void createContactWithDSL() {
        peopleService.createPerson( w->
                w.addAddress(
                        a-> a
                                .country("Spain")
                                .city("Madrid")
                ).addNickname(
                        n-> n
                                .value("PinkiMalinky")
                )
        );
    }

}
